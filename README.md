# hdcpwn

**Download**: https://github.com/xerpi/hdcpwn/releases

**Enable the plugin:**

1. Add hdcpwn.skprx to taiHEN's config (ux0:/tai/config.txt):
	```
	*KERNEL
	ux0:tai/hdcpwn.skprx
	```
2. You need to refresh the config.txt by rebooting or through VitaShell.

#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/lowio/i2c.h>
#include <taihen.h>

/*
* http://www.analog.com/media/en/technical-documentation/user-guides/ADV7511_Programming_Guide.pdf
*/

static tai_hook_ref_t SceI2cForDriver_ksceI2cTransferWrite_ref;
static SceUID SceI2cForDriver_ksceI2cTransferWrite_hook_uid = -1;

static int SceI2cForDriver_ksceI2cTransferWrite_hook_func(int bus, unsigned int addr, const unsigned char *buffer, int size)
{
	unsigned char tmp[2];

	if (bus == 1 && addr == 0x7A && size == 2 && buffer[0] == 0xAF) {
		tmp[0] = 0xAF;
		tmp[1] = (buffer[1] & ~((1 << 7) | (1 << 4))) | (1 << 2);
		buffer = tmp;
	}

	return TAI_CONTINUE(int, SceI2cForDriver_ksceI2cTransferWrite_ref, bus, addr, buffer, size);
}

void _start() __attribute__ ((weak, alias ("module_start")));

int module_start(SceSize argc, const void *args)
{
	SceI2cForDriver_ksceI2cTransferWrite_hook_uid = taiHookFunctionExportForKernel(
		KERNEL_PID,
		&SceI2cForDriver_ksceI2cTransferWrite_ref,
		"SceLowio",
		0xE14BEF6E, /* SceI2cForDriver */
		0xCA94A759, /* ksceI2cTransferWrite */
		SceI2cForDriver_ksceI2cTransferWrite_hook_func);

	static unsigned char buffer[] = {
		0xAF,
		1 << 2,
	};

	ksceI2cTransferWrite(1, 0x7A, buffer, sizeof(buffer));

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	if (SceI2cForDriver_ksceI2cTransferWrite_hook_uid > 0) {
		taiHookReleaseForKernel(SceI2cForDriver_ksceI2cTransferWrite_hook_uid,
			SceI2cForDriver_ksceI2cTransferWrite_ref);
	}

	return SCE_KERNEL_STOP_SUCCESS;
}
